#include "stream_reassembler.hh"

// Dummy implementation of a stream reassembler.

// For Lab 1, please replace with a real implementation that passes the
// automated checks run by `make check_lab1`.

// You will need to add private members to the class declaration in `stream_reassembler.hh`

template <typename... Targs>
void DUMMY_CODE(Targs &&... /* unused */) {}

using namespace std;

StreamReassembler::StreamReassembler(const size_t capacity)
    : _eof(false)
    , _first_unassembled(0)
    , _buffer_size(0)
    , _capacity(capacity)
    , _output(capacity)
    , _buffer(capacity)
    , _exist_data(capacity, false) {}

//! \details This function accepts a substring (aka a segment) of bytes,
//! possibly out-of-order, from the logical stream, and assembles any newly
//! contiguous substrings and writes them into the output stream in order.
void StreamReassembler::push_substring(const string &data, const size_t index, const bool eof) {
    if (eof)
        _eof |= eof;
    /*   if (data.empty() || data.size() + index <= _next_index)
           return;*/
    // size_t length = min(data.size(), remaining_capacity());
    write_to_buffer(data, index);
    string str = "";
    size_t index_tmp = _first_unassembled % _capacity;
    while (_exist_data[index_tmp]) {
        str.push_back(_buffer[index_tmp]);
        _exist_data[index_tmp] = false;
        ++index_tmp;
        index_tmp %= _capacity;
    }
    size_t written = _output.write(str);
    for (size_t i = written; i < str.size(); i++) {
        // these data are not actually written
        _exist_data[(_first_unassembled + i) % _capacity] = true;
    }
    _buffer_size -= written;
    _first_unassembled = _first_unassembled + written;
    if (_eof && _buffer_size == 0)
        _output.end_input();
}

void StreamReassembler::write_to_buffer(const string &data, const size_t &index) {
    // size_t data_size = data.size();
    // size_t l = (index + data_size) - _next_index;
    // if (l <= data_size) {
    //    for (size_t i = 0; i < l; i++) {
    //        if (!_exist_data[(_next_index + i) % _capacity]) {
    //            _buffer[(_next_index + i) % _capacity] = data[data_size - l + i];
    //            _exist_data[(_next_index + i) % _capacity] = true;
    //            ++_buffer_size;
    //        }
    //    }
    //} else {
    //    for (size_t i = 0; i < data_size; i++) {
    //        if (!_exist_data[(index + i) % _capacity]) {
    //            _buffer[(index + i) % _capacity] = data[i];
    //            _exist_data[(index + i) % _capacity] = true;
    //            ++_buffer_size;
    //        }
    //    }
    //}
    size_t offset = max(index, _first_unassembled) - index;
    for (size_t i = offset; i < data.size(); i++) {
        size_t index_tmp = (index + i) % _capacity;
        // make sure not to overwrite the previous data
        if (_exist_data[index_tmp] && _buffer[index_tmp] != data[i])
            break;
        _buffer[index_tmp] = data[i];
        if (!_exist_data[index_tmp]) {
            _exist_data[index_tmp] = true;
            ++_buffer_size;
        }
    }
    // auto iter = _buffer.lower_bound(index);
    // if (iter == _buffer.begin() && iter != _buffer.end()) {
    //    if (index + data.size() < iter->first) {
    //        _buffer[index] = data;
    //        _buffer_size += data.size();
    //    } else {
    //        string str = iter->second.substr((index + data.size() - iter->first), string::npos);
    //        _buffer[index] = data + str;
    //        _buffer_size += str.size() + data.size() - iter->second.size();
    //        _buffer.erase(iter);
    //    }
    //} else {
    //    if (_buffer.empty()) {
    //        if (data.size() > _next_index - index) {
    //            size_t l = data.size() - (_next_index - index);
    //            string str = data.substr(data.size() - l, string::npos);
    //            _buffer[_next_index] = str;
    //            _buffer_size += l;
    //        }
    //    } else {
    //        --iter;
    //        if (iter->first + iter->second.size() < index) {
    //            _buffer[index] = data;
    //            _buffer_size += data.size();
    //        } else {
    //            string str = data.substr(iter->first + data.size() - index, string::npos);
    //            _buffer[iter->first] = iter->second + str;
    //            _buffer_size += str.size();
    //        }
    //    }
    //}
}

size_t StreamReassembler::unassembled_bytes() const { return _buffer_size; }

bool StreamReassembler::empty() const { return _buffer_size == 0; }

// size_t StreamReassembler::remaining_capacity() const { return _capacity - _output.buffer_size() - _buffer_size; }