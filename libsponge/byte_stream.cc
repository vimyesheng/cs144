#include "byte_stream.hh"

#include <algorithm>
#include <iterator>
#include <stdexcept>

// Dummy implementation of a flow-controlled in-memory byte stream.

// For Lab 0, please replace with a real implementation that passes the
// automated checks run by `make check_lab0`.

// You will need to add private members to the class declaration in `byte_stream.hh`

template <typename... Targs>
void DUMMY_CODE(Targs &&... /* unused */) {}

using namespace std;

ByteStream::ByteStream(const size_t capacity)
    : _stream(), _capacity(capacity), _size(0), _written(0), _poped(0), _input_ended(false) {}

size_t ByteStream::write(const string &data) {
    size_t len = min(data.size(), remaining_capacity());
    for (size_t i = 0; i < len; i++) {
        _stream.emplace_back(data[i]);
    }
    _size += len;
    _written += len;
    return len;
}

//! \param[in] len bytes will be copied from the output side of the buffer
string ByteStream::peek_output(const size_t len) const {
    return string(_stream.begin(), _stream.begin() + min(len, _size));
}

//! \param[in] len bytes will be removed from the output side of the buffer
void ByteStream::pop_output(const size_t len) {
    size_t length = min(len, _size);
    for (size_t i = 0; i < length; i++) {
        _stream.pop_front();
    }
    _poped += length;
    _size -= length;
}

void ByteStream::end_input() { _input_ended = true; }

bool ByteStream::input_ended() const { return _input_ended; }

size_t ByteStream::buffer_size() const { return _size; }

bool ByteStream::buffer_empty() const { return _size == 0; }

bool ByteStream::eof() const { return input_ended() && buffer_empty(); }

size_t ByteStream::bytes_written() const { return _written; }

size_t ByteStream::bytes_read() const { return _poped; }

size_t ByteStream::remaining_capacity() const { return _capacity - _size; }
